
============
README.rst
============


Passage de 3.12.1 le jeudi 21 décembre 2023
=======================================================

::

    grep  -R 'python:3.11' --include=".gitlab-ci.yml"
    
 
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.2/python:3.12.1/g'
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.3/python:3.12.1/g'
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.4/python:3.12.1/g'
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.5/python:3.12.1/g'
       
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.0/python:3.11.2/g'


gitlab-ci.yml
--------------------

::

    grep  -R 'python:3.12.0' --include=".gitlab-ci.yml"
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.12.0/python:3.12.1/g'

pyproject.toml
--------------------

::

    grep  -R 'python = "^3.10' --include="pyproject.toml"
    grep  -R 'python = "^3.11' --include="pyproject.toml"

=>::

    find . -name "pyproject.toml" -print0 | xargs -0 sed -i 's/3.10/3.12/g'
    find . -name "pyproject.toml" -print0 | xargs -0 sed -i 's/3.11/3.12/g'

Passage de 3.10.2 à 3.10.3 le vendredi 10 février 2023
=======================================================

::

    grep  -R 'python:3.11' --include=".gitlab-ci.yml"
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.1/python:3.11.2/g'
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.0/python:3.11.2/g'




Passage de 3.10 à 3.11 le vendredi 26 août 2022
===================================================

::

    grep  -R 'python = "^3.10' --include="pyproject.toml"

=>::

    find . -name "pyproject.toml" -print0 | xargs -0 sed -i 's/3.10/3.11/g'


::

    grep  -R 'python:3.11.0rc2' --include=".gitlab-ci.yml"
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.0rc2/python:3.11.0/g'
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.11.0rc1/python:3.11.0rc2/g'



Passage de 3.10.2 à 3.10.3 le lundi 21 mars 2022
=====================================================

::

    grep  -R 'python:3.10' --include=".gitlab-ci.yml"
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.10.2/python:3.10.3/g'
    find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.10.4/python:3.10.6/g'


0) Update .gitignore => invoke listsubdir
