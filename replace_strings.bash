find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.9.1-slim/python:3.9.7-slim-bullseye/g'
find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/master/main/g'
find . -name "pyproject.toml" -print0 | xargs -0 sed -i 's/3.9/3.10/g'
find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.9.7-slim/python:3.10.0-slim-bullseye/g'
find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.9.1-slim/python:3.10.0-slim-bullseye/g'
find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/bullseye-bullseye/bullseye/g'
find . -name ".gitlab-ci.yml" -print0 | xargs -0 sed -i 's/python:3.9.7/python:3.10.0/g'

